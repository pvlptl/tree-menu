interface Node {
  title: string
  nodes: Node[]
}

export default Node
