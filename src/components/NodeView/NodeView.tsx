import React, { memo, useMemo } from 'react'
import styled from 'styled-components'
import Node from '../../types/Node'
import useNodeView from '../../hooks/useNodeView'

const Root: any = styled.div`
  padding: 4px;
  border: 1px solid rebeccapurple;
`

const Typography: any = styled.div`
  padding: 4px;
  &:hover {
    cursor: ${(props: any) => (props.hasNodes ? 'pointer' : 'not-allowed')};
  }
`

function NodeView({ title, nodes }: Node) {
  const { open, hasNodes, onClick: onClickNode } = useNodeView(nodes)

  const renderNodes = useMemo(
    () =>
      open &&
      hasNodes &&
      nodes.map((node: Node, idx: number) => <NodeView key={idx} {...node} />),
    [open]
  )

  return (
    <Root hasNodes>
      <Typography onClick={onClickNode} hasNodes={hasNodes}>
        {hasNodes && '+'} {title}
      </Typography>
      {renderNodes}
    </Root>
  )
}

export default memo(NodeView)
