import { useState } from 'react'
import Node from '../types/Node'

function useNodeView(nodes: Node[]) {
  const [open, setOpen] = useState(false)
  const hasNodes = nodes.length > 0

  const handleClick = () => {
    hasNodes && setOpen(prev => !prev)
  }

  return {
    open,
    hasNodes,
    onClick: handleClick
  }
}

export default useNodeView
