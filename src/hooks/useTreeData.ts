import { useEffect, useState } from 'react'
import Node from '../types/Node'

function useTreeData() {
  const [nodes, setNodes] = useState<Node[]>([])
  const [loading, setLoading] = useState<boolean>(false)

  useEffect(() => {
    function cb() {
      setLoading(true)
      setTimeout(() => {
        setLoading(true)
        const xhttp = new XMLHttpRequest()
        xhttp.onload = function () {
          const response = JSON.parse(this.responseText)
          setNodes(response)
          setLoading(false)
        }
        xhttp.open('GET', '/data.json', true)
        xhttp.send()
      }, 1500)
    }

    cb()
  }, [])

  return {
    nodes,
    loading
  }
}

export default useTreeData
