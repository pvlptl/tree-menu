import React from 'react'
import NodeView from './components/NodeView'
import useTreeData from './hooks/useTreeData'

function App() {
  const { nodes, loading } = useTreeData()

  if (loading) {
    return (
      <div>
        <p>Loading...</p>
      </div>
    )
  }

  return (
    <div>
      {nodes.map((i, idx) => (
        <NodeView key={idx} {...i} />
      ))}
    </div>
  )
}

export default App
